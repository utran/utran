function createHostId() {
  return "host_" + Math.random().toString(36).substr(2, 9);
}
class CancellablePromise<T> {
  // 在这里声明泛型 T
  public readonly promise: Promise<T>;
  private resolve!: (value: T | PromiseLike<T>) => void;
  private reject!: (reason?: any) => void;

  constructor(
    executor: (
      resolve: (value: T | PromiseLike<T>) => void,
      reject: (reason?: any) => void
    ) => void
  ) {
    this.promise = new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
      executor(resolve, reject);
    });
  }
  cancel() {
    this.reject(new Error("Promise was cancelled."));
  }
}

function runWokeBlock(block: WokeBlock) {
  return new CancellablePromise<any>(async (resolve, reject) => {
    try {
      await block.onBeofreStart();
      await block.run();
      await block.onCompleted();
      resolve(null);
    } catch (error: any) {
      if (error instanceof RemoteNotImplementedError) {
        console.error("WokeBlock执行失败,远程action未实现", error);
        await block.onStop();
        resolve(null);
        return;
      }
      if (error instanceof Error) {
        console.error("WokeBlock执行失败", error);
        await block.onError(error);
        resolve(null);
        return;
      }
      console.error("WokeBlock执行失败,未知错误", error);
      reject(error);
    }
  });
}


interface BaseMessagePayload {
  msg_id: string;
  from_host: string[];
  to_host: string;
}

interface FirstMessagePayload extends BaseMessagePayload {
  init_data: {
    imp_ations: string[];
    server_uri?: string;
  };
}

interface MessagePayload extends BaseMessagePayload {
  future_id?: string;
  data?: Record<string, any>;
  confirm?: boolean;
  action?: string;
  error?: string;
}

interface UtranContext {
  hostInstance: Host;
  curImpRemoteActions: Set<string>;
  isAllImpRemoteActions: boolean;
}

const LOCAL_HOST_ID = createHostId();
const CONNECTIONS: Record<string, UtConnection> = {};

const LOCAL_ACTIONS: Record<string, Function> = {};
const REMOTE_IMP_ACTIONS: Record<string, Function> = {};
const WOKEBLOCKS: Map<typeof WokeBlock, WokeBlock> = new Map();

const RUNING_WOKEBLOCKS: Map<WokeBlock,CancellablePromise<any>> = new Map();

export function setisAllImpRemoteActions() {
  const result = new Set(Object.keys(REMOTE_IMP_ACTIONS));
  for (let item of UTRAN_CONTEXT.curImpRemoteActions) {
    result.delete(item);
  }
  UTRAN_CONTEXT.isAllImpRemoteActions = result.size === 0;
  return UTRAN_CONTEXT.isAllImpRemoteActions;
}

const UTRAN_CONTEXT: UtranContext = {
  hostInstance: null!,
  curImpRemoteActions: new Set(),
  isAllImpRemoteActions: false,
};

function onNewConnection(conn: UtConnection) {
  console.log("🔌新连接", conn.hostId);
  // 将当前imp_ations，添加到当前curImpRemoteActions中
  const keys = Object.keys(REMOTE_IMP_ACTIONS);
  conn.imp_ations
    .filter((k) => keys.includes(k))
    .forEach((action_key) => UTRAN_CONTEXT.curImpRemoteActions.add(action_key))

  const is_already = UTRAN_CONTEXT.isAllImpRemoteActions;
  const is_ready = setisAllImpRemoteActions();
  if (!is_already && is_ready) {
      console.log(
        `✅[${UTRAN_CONTEXT.curImpRemoteActions.size}]所有imp_ations都已注册，开始执行WokeBlock`
      );
      if (WOKEBLOCKS.size === 0) {
        console.log("🙈 未注册WokeBlock");
        return;
      }
      setTimeout(() => {
        WOKEBLOCKS.forEach(async (block) => {
          if (RUNING_WOKEBLOCKS.has(block)) {
            console.log("🙈 该WokeBlock正在执行中，跳过");
            return;
          }
          const cancellablePromise = runWokeBlock(block);
          RUNING_WOKEBLOCKS.set(block, cancellablePromise);
          try {
            await cancellablePromise.promise; 
            // block._isCompleted = true;           
          } catch (error: any) {
            if (error.message === "Promise was cancelled.") {
              console.log("执行被取消");
            } else {
              throw error; // 处理其他错误
            }
          } finally {
            RUNING_WOKEBLOCKS.delete(block);
          }
        });
      }, 0);
  }
}

function onConnectionClose(conn: UtConnection) {
  console.log("🔌连接断开", conn.hostId);
  // 将当前imp_ations，从当前curImpRemoteActions中移除
  conn.imp_ations.forEach((action_key) =>
    UTRAN_CONTEXT.curImpRemoteActions.delete(action_key)
  );
  const is_already = UTRAN_CONTEXT.isAllImpRemoteActions;
  const is_ready = setisAllImpRemoteActions();

  if (is_already && !is_ready) {
    RUNING_WOKEBLOCKS.forEach((promise, block) => {
      promise.cancel();
      block.onStop()
    });
    RUNING_WOKEBLOCKS.clear();
    console.log("✅所有imp_ations都已注册，停止执行WokeBlock");
  }

}

var fu_num = 0;
const ALL_FUTURES: Map<UtConnection, { [future_id: string]: Future }> =
  new Map();

class Future<T = any> {
  promise: Promise<T>;
  readonly id: string;
  constructor() {
    this.id = "fu_" + fu_num++;
    this.promise = new Promise((resolve, reject) => {
      this.setResult = resolve;
      this.setError = reject;
    });
  }
  setResult(_: T) {}
  setError(_?: string) {}
}

// export function registerRemoteAction(action_key: string, action: Function) {
//   REMOTE_IMP_ACTIONS[action_key] = action;
// }

/**
 * 调用本地action
 * @param {string} action_key
 * @param {Record<string, any>} data
 * @returns
 */
async function callLocalAction(
  action_key: string,
  data: { kwargs: Record<string, any>; args: any[] }
) {
  console.debug("callLocalAction", action_key, data);
  const action = LOCAL_ACTIONS[action_key];
  if (!action) return { error: "未注册的本地action: " + action_key };
  try {
    return {
      result: await action(...data.args, data.kwargs),
    };
  } catch (error) {
    return { error: String(error) };
  }
}

class IPC_Handler {
  private _conn: UtConnection;
  private _isFirstMsg: boolean;
  constructor(conn: UtConnection) {
    this._conn = conn;

    conn.send({
      // 发送首次消息
      msg_id: conn.newMsgId(),
      from_host: [LOCAL_HOST_ID],
      to_host: "首次消息",
      init_data: {
        imp_ations: Object.keys(LOCAL_ACTIONS),
      },
    });

    this._isFirstMsg = true;
  }

  async onFirstMessage(payload: FirstMessagePayload) {
    if (!this._isFirstMsg) return false;
    const from_host = payload["from_host"];
    const init_data = payload["init_data"];
    const imp_ations = init_data["imp_ations"];
    const server_uri = init_data["server_uri"];

    if (from_host.length > 1) {
      throw new Error("非法连接, 首次消息中from_host长度大于1");
    }

    if (from_host[0] == LOCAL_HOST_ID) {
      throw new Error("非法连接, 不合法host_id");
    }
    this._isFirstMsg = false;
    this._conn.setInitData(from_host[0], imp_ations, server_uri);
    CONNECTIONS[this._conn.hostId] = this._conn;
    onNewConnection(this._conn);
    return true;
  }

  async onLocalMessage(payload: MessagePayload) {
    const to_host = payload["to_host"];
    if (to_host !== LOCAL_HOST_ID) return false;

    const from_host = payload["from_host"];
    const msg_id = payload["msg_id"];
    const action_key = payload["action"];
    const future_id = payload["future_id"];
    const confirm = payload["confirm"];

    if (from_host[0] == LOCAL_HOST_ID) {
      // 处理自发的消息
      if (future_id) {
        // 完成本地future
        const future_map = ALL_FUTURES.get(this._conn) as {
          [future_id: string]: Future;
        };
        const future = future_map[future_id];
        if (future) {
          future.setResult(payload["data"]);
          delete future_map[future_id];
        }
      }
      return true;
    }

    from_host.push(LOCAL_HOST_ID); // 添加本地host_id
    const is_reply = future_id || confirm;

    if (action_key) {
      const data = (payload["data"] || { kwargs: {}, args: [] }) as {
        kwargs: Record<string, any>;
        args: any[];
      };
      const res = await callLocalAction(action_key, data);
      if (res.error) {
        this._conn.send({
          msg_id: msg_id,
          from_host: from_host,
          to_host: from_host[0],
          error: res.error,
        });
        return true;
      }

      if (is_reply) {
        this._conn.send({
          msg_id: msg_id,
          from_host: from_host,
          data: res.result,
          future_id: future_id,
          to_host: from_host[0],
        });
      }
      return true;
    }

    if (is_reply) {
      this._conn.send({
        ...payload,
        from_host: from_host,
        to_host: from_host[0],
      });
    }
    return true;
  }

  /**
   * 仅处理转发的消息，且确保只转发一次
   * @param payload
   * @returns
   */
  async onTranspondMessage(payload: MessagePayload) {
    const to_host = payload["to_host"];
    if (to_host == LOCAL_HOST_ID || !to_host) return false;
    console.debug("onTranspondMessage", payload);
    const from_host = payload["from_host"];
    if (from_host.includes(LOCAL_HOST_ID)) return true; // 确保只转发一次

    from_host.push(LOCAL_HOST_ID); // 添加本地host_id

    const target_host_id = to_host;
    if (target_host_id in CONNECTIONS) {
      const conn = CONNECTIONS[target_host_id];
      conn.send({ ...payload, from_host: from_host });
      console.debug("🔁消息成功转发到指定host");
    } else {
      Object.values(CONNECTIONS).forEach((conn) => {
        if (conn != this._conn) {
          conn.send({ ...payload, from_host: from_host });
          console.debug("📢广播转发消息-> to:", conn.hostId);
        }
      });
    }
    return true;
  }
}

/**
 * 消息接收处理分发中心
 * @param {UtConnection} conn
 */
async function* MsgDispatchHubGenerator(conn: UtConnection) {
  const ipc_handler = new IPC_Handler(conn);
  while (true) {
    const payload: MessagePayload | FirstMessagePayload = yield;
    try {
      if (await ipc_handler.onFirstMessage(payload as FirstMessagePayload))
        continue; // 处理首次消息
      if (conn.hostId !== payload.from_host[payload.from_host.length - 1]) {
        console.error("消息来源不合法, 连接的host_id与消息来源的host_id不一致");
        break; // 终止处理
      }

      if (await ipc_handler.onLocalMessage(payload)) continue; // 处理本地消息
      if (await ipc_handler.onTranspondMessage(payload)) continue; // 处理转发消息
    } catch (error) {
      console.error(error);
      break;
    }
  }
}

class UtConnection {
  _msg_num = 0;
  private readonly _local_host_id = LOCAL_HOST_ID;
  private _host_id: string | undefined;
  private _ws: WebSocket;
  private _dispatch_hub: ReturnType<typeof MsgDispatchHubGenerator>;
  private _imp_ations: Array<string> = [];
  private _server_uri: string | undefined;
  constructor(readonly uri = "ws://localhost:2525/utran") {
    const ws = new WebSocket(uri);
    const dispatch_hub = MsgDispatchHubGenerator(this);
    const conn = this;

    ws.onopen = function () {
      ALL_FUTURES.set(conn, {});
      dispatch_hub.next(); // 启动消息接收处理分发中心
      console.info("connected to:", conn.uri);
    };
    ws.onmessage = function (event) {
      console.debug("onmessage", event.data);
      try {
        const playload = JSON.parse(event.data);
        dispatch_hub.next(playload);
      } catch (error) {
        console.warn("onmessage error: ", error);
      }
    };
    ws.onerror = function (error) {
      console.error("onerror", error);
    };
    ws.onclose = function () {
      dispatch_hub.return(); // 关闭消息接收处理分发中心
      const padding_futures = Object.values(ALL_FUTURES.get(conn) ?? {});
      padding_futures.forEach((future) => future.setError("连接已断开"));
      ALL_FUTURES.delete(conn);
      if (conn.hasReady) delete CONNECTIONS[conn.hostId];
      console.warn("closed! clearned connection to:", conn.uri);
      onConnectionClose(conn);
    };

    this._ws = ws;
    this._dispatch_hub = dispatch_hub;
  }

  get hasReady() {
    return this._host_id !== undefined;
  }

  get hostId() {
    if (!this._host_id) throw new Error("host_id is not set");
    return this._host_id;
  }

  get imp_ations() {
    return this._imp_ations;
  }

  get server_uri() {
    return this._server_uri;
  }

  newMsgId() {
    this._msg_num++;
    return "msg_" + this._local_host_id + "_" + this._msg_num;
  }

  setInitData(host_id: string, imp_ations: string[], server_uri?: string) {
    this._host_id = this._host_id ? this._host_id : host_id;
    this._imp_ations = Array.from(new Set(imp_ations));
    this._server_uri = server_uri;
  }

  close() {
    this._ws.close();
    this._dispatch_hub.return();
  }

  send(data: MessagePayload | FirstMessagePayload) {
    this._ws.send(JSON.stringify(data));
  }
}

/**
 * 远程action未实现错误
 */
class RemoteNotImplementedError extends Error {}

export class Host {
  constructor(...uris: string[]) {
    UTRAN_CONTEXT.hostInstance = this;
    WOKEBLOCKS.clear();
    WOKEBLOCKS.forEach((block) => {
      if (block.useRemoteActions.length === 0) {
        block.useRemoteActions = Object.values(REMOTE_IMP_ACTIONS);
      }
    });
    uris.forEach((uri) => {
      this.createActConnection(uri);
    });
  }

  createActConnection(uri: string): boolean {
    let all_connections = Object.values(CONNECTIONS);
    for (let i = 0; i < all_connections.length; i++) {
      const conn = all_connections[i];
      if (conn.uri === uri) return false;
    }
    new UtConnection(uri);
    return true;
  }

  /**
   * 调用远程action
   * @param action_key
   * @param data
   * @returns
   */
  callRemoteAction<T = unknown>(
    action_key: string,
    data: { kwargs: Record<string, any>; args: any[] }
  ) {
    let all_connections = Object.values(CONNECTIONS);
    let target_conns = all_connections.filter((conn) =>
      conn.imp_ations.includes(action_key)
    );
    if (target_conns.length === 0) {
      return Promise.reject(new RemoteNotImplementedError("远程无可调用实现"));
    }

    let future = new Future<T>();
    let future_id = future.id;

    const conn = target_conns[0]; // 选择第一个连接作为目标连接
    let msg_id = conn.newMsgId();
    const payload: MessagePayload = {
      msg_id: msg_id,
      from_host: [LOCAL_HOST_ID],
      to_host: conn.hostId,
      action: action_key,
      data: data,
      future_id: future_id,
    };
    conn.send(payload);

    const future_map = ALL_FUTURES.get(conn) as { [future_id: string]: Future };
    future_map[future_id] = future;

    return future.promise;
  }
}

export abstract class WokeBlock {
  _isCompleted: boolean;
  constructor(){
    this._isCompleted = false;
  }
  useRemoteActions: Array<Function> = [];
  abstract run(): void;
  onBeofreStart(): void {}
  onStop(): void {}
  onCompleted(): void {}
  onError(error: Error): void {
    console.error(error);
  }
}

export function registerWokeBlock(block_class: new () => WokeBlock) {
  let w = WOKEBLOCKS.get(block_class);
  if (!w) WOKEBLOCKS.set(block_class, new block_class());
  else console.warn(`WokeBlock ${block_class.name} 已注册`);
}

export function registerLocalAction(action_key: string, action: Function) {
  LOCAL_ACTIONS[action_key] = action;
}

export function registerRemoteAction(action_key: string, action: Function) {
  REMOTE_IMP_ACTIONS[action_key] = action;
}
