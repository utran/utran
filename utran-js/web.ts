
import { Host, registerLocalAction,WokeBlock,registerWokeBlock } from "./src";

// 测试用例
function remote_add(a: number, b: number) {
  return a + b;
}
registerLocalAction("remote_add", remote_add);

const host = new Host("ws://localhost:2525/utran");



class TestBlock extends WokeBlock {
  async run(){
    console.log("test block run");
    let result = await host.callRemoteAction("test_action", {args: [1, 2], kwargs: {}})
    console.log(result);

    let result2 = await host.callRemoteAction("test_action2", {args: [1, 2], kwargs: {}})

  }
  async onStop(){
    console.log("test block stop");
  }
  async onCompleted(){
    console.log("test block complete");
  }
  async onError(error: any){
    console.log("test block error", error);
  }
}


registerWokeBlock(TestBlock)


