
# 将工作区的utran包添加到环境变量中
import random
import sys, os
directory = os.path.dirname(os.path.dirname(__file__))
sys.path.append(directory)


import utran

@utran.remote_imp(timeout=30, cache=utran.UtCaches.LFUCache(10),lock=True)
async def remote_add(a: int, b: int) -> int:...


# AppServer
app = utran.AppServer()
utran.set_server_instance(app)
@app.get('/')
async def home(r):
    try:
        result = await remote_add(2,3)
        return result
    except utran.UtExceptions.RemoteRunActionError as e:
        return str(e)

@app.get('/stop')
async def stop(r):
    print("Server to stoped")
    utran.Host.instance().exit()
    return "Server stoped"


@app.get('/delay')
async def get_delay(r):
    return await utran.getConnectionsDelay()


# 启动方法1：
# utran.run(host='127.0.0.1',port=random.randint(2000,3000))


# 启动方法2：
utran.start_host()
app.run(host='127.0.0.1',port=random.randint(2000,3000))