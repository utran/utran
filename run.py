"""
# 打包whl文件,并发布到pipy.org
使用方法:
1. 安装wheel和setuptools (pip install wheel setuptools)
2. python setup.py bdist_wheel
3. python -m twine upload ./dist/ultrarpc-0.5.5-py3-none-any.whl
> 注: 上传前需要 配置~/.pypirc文件  并将文件放到用户目录下 例如: C:\Users\Administrator\.pypirc
4. git tag | grep 'v' | xargs -n 1 -I% git push origin %    推送tag
"""

import os
import subprocess


def get_last_whl_file(whl_dir:str)->str:
    dist_path = os.path.abspath(whl_dir)
    files = os.listdir(dist_path)
    last_whl_file = ''
    _max = 0
    for f in files:
        _whl_file = '\\'.join([dist_path,f])
        t = os.path.getmtime(_whl_file)
        if t>_max:
            _max = t
            last_whl_file = _whl_file
    if not last_whl_file:
        raise RuntimeError("没有找到.whl文件")
    return last_whl_file
