# python版本最低要求 python 3.10+


- `list[str] | tuple[str]` 这种组合语法是在Python 3.10引入的“联合类型”语法才支持。
- `typing.get_origin` 函数是从 python 3.8 引入
- `typing.get_args` 函数是从 python 3.8 引入
- `typing.get_type_hints` 函数是从 python 3.7 引入