import asyncio
import html
from typing import Optional
from aiohttp import web
import aiohttp
from fastapi import FastAPI

# 将工作区的utran包添加到环境变量中
import sys, os
directory = os.path.dirname(os.path.dirname(__file__))
sys.path.append(directory)

import utran

@utran.action
def multiply(a: int, b: int) -> int:
    return a * b

@utran.action
def add(a: int, b: int) -> int:
    return a + b

@utran.remote_imp(timeout=30, cache=utran.LFUCache(10),lock=True)
async def remote_add(a: int, b: int) -> int:...



# fastapi后端
app = FastAPI()
utran.set_server_instance(app)
@app.get('/')
async def home():
    try:
        result = await remote_add(2,3)
        return result
    except utran.UtExceptions.RemoteRunActionError as e:
        return str(e)
    
@app.get('/stop')
async def stop():
    print("Server to stoped")
    utran.Host.instance().exit()
    return web.Response(text="Server stoped")


# aiohttp后端
# app = web.Application()
# utran.set_server_instance(app)

# async def home(request):
#     try:
#         result = await remote_add(2,3)
#         return web.Response(text=str(result))
#     except utran.UtExceptions.RemoteRunActionError as e:
#         return web.Response(text=str(e))

# async def original(request):
#     loop = asyncio.get_event_loop()
#     # 获取所有得task
#     tasks = asyncio.all_tasks(loop)
#     # 格式化打印task
    
#     lis_task = [(f'{task.get_name()} ==> {task.get_coro().__name__}\n\n{task.get_stack()}') for task in tasks]
#     return web.Response(text=html.escape('\n'.join(lis_task)))
    
# async def stop(request):
#     print("Server to stoped")
#     utran.Host.instance().exit()
#     return web.Response(text="Server stoped")

# app.router.add_get('/',home)
# app.router.add_get('/aiohttp',original)
# app.router.add_get('/stop',stop)



# # 默认后端
# app = utran.AppServer(ping_interval=20)
# utran.set_server_instance(app)

# @app.get('/')
# async def home(request):
#     try:
#         result = await remote_add(2,3)
#         asyncio.run_coroutine_threadsafe
#         print(result)
#         return result
#     except utran.UtExceptions.RemoteRunActionError as e:
#         return str(e)

# @app.get('/delay')
# async def get_delay(request):
#     return await utran.getConnectionsDelay()

# @app.get('/tasks')
# async def tasks(request):
#     loop = asyncio.get_event_loop()
#     # 获取所有得task
#     tasks = asyncio.all_tasks(loop)
#     res = {}
#     for task in tasks:
#         res[task.get_name()] = {
#             "coro_name": task.get_coro().__name__, # type: ignore
#             "stack": [str(item) for item in task.get_stack()]
#         }
#     return res

# @app.get('/stop')
# def stop(request):

#     utran.Host.instance().exit()
#     return "Server stoped"





utran.run()

