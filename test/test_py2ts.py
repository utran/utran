
from collections import OrderedDict, defaultdict
from dataclasses import dataclass
import typing

# 将工作区的utran包添加到环境变量中
import sys, os
directory = os.path.dirname(os.path.dirname(__file__))
sys.path.append(directory)

from utran.py_ts import convert_class_to_ts, convert_function_to_ts, map_base_type, map_typeVar_type,convert_to_ts



# 测试单一类型
print("int => ", map_base_type(int))  # number
print("float => ", map_base_type(float))  # number
print("str => ", map_base_type(str))  # string
print("bool => ", map_base_type(bool))  # boolean
print("bytes => ", map_base_type(bytes))  # Uint8Array
print("complex => ", map_base_type(complex))  # string
print("range => ", map_base_type(range))  # number[]
print("None => ", map_base_type(type(None)))  # null

# 测试原始复合类型
print("int|str => ", map_base_type((int | str)))  #  number | string
print("(int, str) => ", map_base_type((int, str)))  #  [number , string]
print("[int, str] => ", map_base_type([int, str]))  #  [number , string]
print("list => ", map_base_type(list))  #  any[]
print("list => ", map_base_type(list[int]))  # Array<number>
print("list => ", map_base_type(list[int | str]))  # Array<number | string>
print("tuple[int, str] => ", map_base_type(tuple[int, str]))  # [number , string]
print("dict[str, int] => ", map_base_type(dict[str, int]))  # Record<string, number>
print("Mapping => ", map_base_type(typing.Mapping[str, int]))  # Record<string, number> 
print("MutableMapping => ", map_base_type(typing.MutableMapping[str, int]))  # Record<string, number> 

print("dict => ", map_base_type(dict))  # Record<string, number>
print(
    "OrderedDict => ", map_base_type(OrderedDict[str, int])
)  # Record<string, number>
print(
    "defaultdict => ", map_base_type(defaultdict[str, int])
)  # Record<string, number>
print(
    "OrderedDict => ", map_base_type(typing.OrderedDict[str, int])
)  # Record<string, number>
print(
    "defaultdict => ", map_base_type(typing.DefaultDict[str, int])
)  # Record<string, number>

print("OrderedDict => ", map_base_type(OrderedDict))  # Record<string, any>
print("defaultdict => ", map_base_type(defaultdict))  # Record<string, any>
print("OrderedDict => ", map_base_type(typing.OrderedDict))  # Record<string, any>
print("defaultdict => ", map_base_type(typing.DefaultDict))  # Record<string, any>

print("set[int] => ", map_base_type(set[int]))  # Set<number>
print("set => ", map_base_type(set))  # Set<number>
print("frozenset => ", map_base_type( typing.MutableSet[int]))  # Set<number>
print("typeVar => ", map_typeVar_type(typing.TypeVar("T", bound=(int | str))))  # any
print("typeVar => ", map_typeVar_type(typing.TypeVar("T", int, float, str)))  # number
print("typeVar => ", map_typeVar_type(typing.TypeVar("T", str, int)))  # string

# 测试 typing 模块的复合类型
print("TypeVar => ", map_base_type(typing.TypeVar("T")))  # T
print("Union => ", map_base_type(typing.Union[int, str]))  # number | string
print("Optional => ", map_base_type(typing.Optional[int]))  # number | undefined
print("Literal => ", map_base_type(typing.Literal[1, 2, 3]))  # 1 | 2 | 3
print(
    "TypedDict => ",
    map_base_type(typing.TypedDict("MyDict", {"name": str, "ps": typing.Optional[int]})),
)  # interface MyDict { name: string; age: number; }
print(
    "TypedDict => ", map_base_type(typing.TypedDict)
)  # interface MyDict { name: string; age: number; }
print("NewType => ", map_base_type(typing.NewType("UserId", int)))  # type UserId = number;
print(
    "Callable => ", map_base_type(typing.Callable[[int, str], None])
)  # (...args:[number, string]) => boolean;

# 测试泛型类
@dataclass
class MyClass(list[int]): 
    name:str
    age:int

mc = MyClass(name="test", age=18)
mc.append(4)
print(
    "MyClass => ", map_base_type(MyClass)
)  # interface MyClass extends Array<number> {}
print(
    "MyClass => ", convert_to_ts(MyClass)
)  # interface MyClass extends Array<number> {}
print(
    "MyClass => ", convert_to_ts(mc)
)  # interface MyClass extends Array<number> {}


# 测试函数类型注解
def my_func(a: int, b: str) -> bool: ...

print(
    "my_func => ", convert_to_ts(my_func)
)  # (a: number, b: string) => boolean;




from typing import TypeVar, get_type_hints, Union, Optional, Literal, NewType, TypedDict, Final, ClassVar, Callable, get_origin, get_args

@dataclass
class Person:
    name: str
    age: Optional[int]
    friends: list[str]
    attributes: dict[str, Union[str, int]]
    role: Literal["admin", "user", "guest"]
    score: Final[int] = 100
    id: ClassVar[int]

class MyTypedDict(TypedDict):
    key1: str
    key2: int

UserID = NewType('UserID',int)

Fn = TypeVar('Fn', bound=Callable[[str], int])


def greet(person: Person, times: int) -> str:
    return f"Hello, {person.name}"

def multiply(a: int, b: int) -> int:
    return a * b

# 调用转换函数
print(convert_to_ts(Person))        # 将Person类转换为TypeScript
print(convert_to_ts(MyTypedDict))  # 将TypedDict转换为TypeScript
print(convert_to_ts(UserID))     # 将NewType转换为TypeScript
print(convert_to_ts(greet))      # 将greet函数转换为TypeScript
print(convert_to_ts(multiply))   # 将multiply函数转换为TypeScript
