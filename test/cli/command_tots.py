
# 将工作区的utran目录添加到系统路径中
import sys, os
directory = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
sys.path.append(directory)


import utran as ut

from dataclasses import dataclass
from typing import (
    TypeVar,
    get_type_hints,
    Union,
    Optional,
    Literal,
    NewType,
    TypedDict,
    Final,
    ClassVar,
    Callable,
    get_origin,
    get_args,
)


@dataclass
class Person:
    name: str
    age: Optional[int]
    friends: list[str]
    attributes: dict[str, Union[str, int]]
    role: Literal["admin", "user", "guest"]
    score: Final[int] = 100
    id: ClassVar[int]


class MyTypedDict(TypedDict):
    key1: str
    key2: int


UserID = NewType("UserID", int)

Fn = TypeVar("Fn", bound=Callable[[], int])


@ut.action
def greet(person: Person, times: Optional[int]) -> str:
    return f"Hello, {person.name}"


@ut.action
def multiply(a: int, b: int, c: Fn) -> int:
    return a * b


multiply(2, 3, lambda x: int(x))


