import html
from typing import Optional
from aiohttp import web
from fastapi import FastAPI


try:
    import utran
except ImportError:
    import sys, os

    directory = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
    sys.path.append(directory)
    import utran



@utran.action
def multiply(a: int, b: int) -> int:
    return a * b

@utran.action
def add(a: int, b: int) -> int:
    return a + b

@utran.remote_imp
async def remote_add(a: int, b: int) -> int:...


# server = FastAPI()
# utHost =utran.Host(backend_server = server,backend_type="fastapi")
# @server.get('/')
# async def home():
#     try:
#         result = remote_add(2,3)
#         return web.Response(text=str(result))
#     except utran.exceptions.RemoteRunActionError as e:
#         print(e)
#         return web.Response(text=str(e))

app = utran.AppServer()
utHost =utran.Host(app)

@app.get('/')
async def home(request):
    try:
        result = await remote_add(2,3)
        return result
    except utran.UtExceptions.RemoteRunActionError as e:
        return str(e)
    
# if __name__ == "__main__":
#     utran.run()