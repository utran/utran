declare namespace Utranclient {
  type Fn = (...args:any[]) => number
  interface Person {
  name: string;
  age?: number | null | undefined;
  friends: Array<string>;
  attributes: Record<string, string | number>;
  role: 'admin' | 'user' | 'guest';
  score: number;
  id: number;
}
  function greet(person: Person, times?: number | null | undefined): string
  function multiply(a: number, b: number, c: Fn): number
}