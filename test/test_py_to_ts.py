# test_py_to_ts.py
from collections import OrderedDict, defaultdict
import typing
import unittest
from typing import (
    List,
    Dict,
    NewType,
    Set,
    Tuple,
    TypeVar,
    TypedDict,
    Union,
    Optional,
    Callable,
)


# 将工作区的utran包添加到环境变量中
import sys, os
directory = os.path.dirname(os.path.dirname(__file__))
sys.path.append(directory)


from utran.py_ts import (
    map_newType_type,
    map_typeVar_type,
    map_typedDict_type,
    map_base_type,
    convert_typedDict_to_ts,
)  # 替换为你的模块名称

class TestPyToTsConversion(unittest.TestCase):

    def test_single_types(self):
        self.assertEqual(map_base_type(int), "number")
        self.assertEqual(map_base_type(float), "number")
        self.assertEqual(map_base_type(str), "string")
        self.assertEqual(map_base_type(bool), "boolean")

    def test_list_type(self):
        self.assertEqual(map_base_type(list), "any[]")
        self.assertEqual(map_base_type(List), "any[]")
        self.assertEqual(map_base_type(List[int]), "Array<number>")
        self.assertEqual(map_base_type(List[str]), "Array<string>")
        self.assertEqual(map_base_type(list[int | str]), "Array<number | string>")
        self.assertEqual(map_base_type(List[int | str]), "Array<number | string>")
        self.assertEqual(map_base_type([int | str]), "[number | string]")
        self.assertEqual(map_base_type([int, str]), "[number, string]")

    def test_tuple_type(self):
        self.assertEqual(map_base_type(tuple), "any[]")
        self.assertEqual(map_base_type(Tuple), "any[]")
        self.assertEqual(map_base_type(tuple[int]), "[number]")
        self.assertEqual(map_base_type(Tuple[str]), "[string]")
        self.assertEqual(map_base_type(tuple[int, str]), "[number, string]")
        self.assertEqual(map_base_type(Tuple[int, str]), "[number, string]")
        self.assertEqual(map_base_type((int, str)), "[number, string]")

    def test_set_type(self):
        expectation1 = "Set<any>"
        expectation2 = "Set<number>"
        
        self.assertEqual(map_base_type(set), expectation1)
        self.assertEqual(map_base_type(Set), expectation1)
        self.assertEqual(map_base_type(set[int]), expectation2)
        self.assertEqual(map_base_type(Set[int]), expectation2)
        
        self.assertEqual(map_base_type(typing.FrozenSet), expectation1)
        self.assertEqual(map_base_type(typing.FrozenSet[int]), expectation2)
        
        self.assertEqual(map_base_type(frozenset), expectation1)
        self.assertEqual(map_base_type(frozenset[int]), expectation2)
        
        self.assertEqual(map_base_type(typing.MutableSet), expectation1)
        self.assertEqual(map_base_type(typing.MutableSet[int]), expectation2)
        

    def test_dict_type(self):
        expectation1 = "Record<string, any>"
        expectation2 = "Record<string, number>"

        self.assertEqual(map_base_type(dict), expectation1)
        self.assertEqual(map_base_type(Dict), expectation1)
        self.assertEqual(map_base_type(dict[str, int]), expectation2)
        self.assertEqual(map_base_type(Dict[str, int]), expectation2)

        self.assertEqual(map_base_type(OrderedDict), expectation1)
        self.assertEqual(map_base_type(typing.OrderedDict), expectation1)
        self.assertEqual(map_base_type(OrderedDict[str, int]), expectation2)
        self.assertEqual(map_base_type(typing.OrderedDict[str, int]), expectation2)
        
        self.assertEqual(map_base_type(defaultdict), expectation1)
        self.assertEqual(map_base_type(typing.DefaultDict), expectation1)
        self.assertEqual(map_base_type(defaultdict[str, int]), expectation2)
        self.assertEqual(map_base_type(typing.DefaultDict[str, int]), expectation2)
        
        
    def test_typedDict_type(self):
        self.assertEqual(
            map_typedDict_type(
                TypedDict("MyTypedDict", {"name": str, "age": int}),
            ),
            "{\n  name: string;\n  age: number;\n}",
        )
        self.assertEqual(
            map_base_type(TypedDict("MyTypedDict", {"name": str, "age": int})),
            "MyTypedDict",
        )

    def test_union_type(self):
        self.assertEqual(map_base_type(int | str), "number | string")
        self.assertEqual(map_base_type(Union[int, str]), "number | string")

    def test_optional_type(self):
        self.assertEqual(map_base_type(Optional[int]), "number | null | undefined")
        self.assertEqual(
            map_base_type(Optional[int | list[int | str]]),
            "number | Array<number | string> | null | undefined",
        )

    # def test_typed_dict(self):
    #     class MyTypedDict:
    #         name: str
    #         age: int

    #     expected_output = "interface MyTypedDict {\n  name: string;\n  age: number;\n}"
    #     self.assertEqual(convert_typedDict_to_ts(MyTypedDict), expected_output)

    def test_callable_type(self):
        def my_function(x: int, y: str) -> None: ...

        self.assertEqual(
            map_base_type(Callable[[int, str], None]),
            "(...args:[number, string]) => void",
        )
        self.assertEqual(
            map_base_type(Callable[[int, Optional[str | int]], tuple[int, str]]),
            "(...args:[number, string | number | null | undefined]) => [number, string]",
        )

        self.assertEqual(
            map_base_type(Callable[[int, str], Optional[int]]),
            "(...args:[number, string]) => number | null | undefined",
        )

    def test_newType_types(self):
        self.assertEqual(map_base_type(NewType("UserId", int)), "UserId")

        self.assertEqual(
            map_newType_type(NewType("UserId", int)),
            "number",
        )

    def test_typeVar_types(self):
        self.assertEqual(map_base_type(TypeVar("T")), "T")
        self.assertEqual(
            map_typeVar_type(TypeVar("T", bound=(int | str))), "number | string"
        )
        self.assertEqual(
            map_typeVar_type(TypeVar("T", int, float, str)), "number | number | string"
        )
        self.assertEqual(map_typeVar_type(TypeVar("T", str, int)), "string | number")


if __name__ == "__main__":
    unittest.main()
