import asyncio
import time
import utran
from utran import UtWokeBlock

from aiohttp import web
# app = web.Application()
app = utran.AppServer(ping_interval=20)
utran.set_server_instance(app)


@app.get('/delay')
async def get_delay(request):
    return await utran.getConnectionsDelay()

@app.get('/tasks')
async def tasks(request):
    loop = asyncio.get_event_loop()
    # 获取所有得task
    tasks = asyncio.all_tasks(loop)
    res = {}
    for task in tasks:
        res[task.get_name()] = {
            "coro_name": task.get_coro().__name__, # type: ignore
            "stack": [str(item) for item in task.get_stack()]
        }
    return res


@utran.action
async def test_action(a:int, b:int)->int:
    return a+b


@utran.remote_imp
async def remote_add(a:int, b:int)->int:...

@utran.remote_imp
async def remote_add2(a:int, b:int)->int:...

# class MyWokeBlock(UtWokeBlock.WokeBlockTemplate):
    
#     # woke_mode = 'close_conn'
    
#     @UtWokeBlock.mark_remote_imp()
#     async def remote_add(self, a:int, b:int)->int:...
        
#     async def on_before_start(self):
#         print("开始之前")

#     def on_stop(self):
#         print("停止")

#     def on_error(self, error):
#         print("错误", error)

#     def on_complate(self):
#         print("完成")
#         # raise RuntimeError("test error")
    
#     async def run(self):
#         # res = await self.remote_add(3, 2)
#         # print("run result:",res)
#         try:
#             res = await MyWokeBlock().remote_add(3, 2)
#             print("run result:",res)
#             res = await self.remote_add(1, 2)
#             print("run result:",res)
#         except Exception as e:
#             print("发生错误： ",e)



# @utran.wokeblock_func(use_remote_actions=(remote_add,),scope_remote_actions=utran.r_scope())
# async def woke_block_func():
#     res = await remote_add(1, 2)
#     print("woke_block_func result:",res)
#     res = await remote_add(3, 4)
#     print("woke_block_func result2:",res)
#     all = await asyncio.gather(*[remote_add(i, 88) for i in range(10)],return_exceptions=False)
#     print("woke_block_func all:",all)
#     for i in all:
#         print(i)




@utran.wokeblock_func(woke_mode='only_new_conn')
async def woke_block_func():
    print("woke_block_func new_conn")


class MyWokeBlock2(UtWokeBlock.WokeBlockTemplate):
    woke_mode = 'only_new_conn'
    
    async def run(self):
        print("MyWokeBlock2 run")
        while True:
            await asyncio.sleep(2)
            print("MyWokeBlock2 run...")
            
    
    def on_stop(self):
        print("MyWokeBlock2 stop")
        raise RuntimeError("test on_stop verror")





        
utran.run(_is_dev_mode=True)
# utran.run(_is_dev_mode=True)
