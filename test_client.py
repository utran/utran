import random
import time
from fastapi import FastAPI
from aiohttp import web

# 将工作区的utran包添加到环境变量中
import sys, os
directory = os.path.dirname(os.path.dirname(__file__))
sys.path.append(directory)


import utran

@utran.action()
def remote_add(a: int, b: int) -> int:
    time.sleep(1)
    print("remote_add called")
    return a+b


# remote_add(2,3)


# server = FastAPI()
# utHost =utran.Host(backend_server = server,backend_type="fastapi")
# @server.get('/')
# async def home():
#     try:
#         result = remote_add(2,3)
#         return web.Response(text=str(result))
#     except utran.exceptions.RemoteRunActionError as e:
#         print(e)
#         return web.Response(text=str(e))
    

utHost =utran.Host.add_connect_uri('ws://127.0.0.1:2525/utran')
app = utran.AppServer()
utran.set_server_instance(app)

# @app.get('/delay')
# async def get_delay(request):
#     return await utran.getConnectionsDelay()


# @utHost.backend.get('/')
# async def home(request):
#     try:
#         result = remote_add(2,3)
#         return result
#     except utran.exceptions.RemoteRunActionError as e:
#         return str(e)


# 启动方式1：
# utran.run(host='127.0.0.1',port=random.randint(2000,3000))


# 启动方式2：
utran.start_host()
app.run(host='127.0.0.1',port=random.randint(2000,3000))