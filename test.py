# import signal
# import threading
# event = threading.Event()

# def signal_handler(sig, frame):
#     print('You pressed Ctrl+C!')
#     event.set()

# signal.signal(signal.SIGINT, signal_handler)
# signal.signal(signal.SIGTERM, signal_handler)


# while not event.is_set():
#     print('Waiting for signal...')
#     event.wait(3)

# print('Exiting...')


import asyncio

loop = asyncio.new_event_loop()

async def main():
    while True:
        print('Waiting for signal...')
        await asyncio.sleep(3)
        loop.stop()

try:
    loop.run_until_complete(main())
except:
    pass



loop.run_until_complete(main())