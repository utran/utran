
# 将工作区的utran包添加到环境变量中
import random
import sys, os
directory = os.path.dirname(os.path.dirname(__file__))
sys.path.append(directory)


from aiohttp import web
import utran

@utran.remote_imp(timeout=30, cache=utran.UtCaches.LFUCache(10),lock=True)
async def remote_add(a: int, b: int) -> int:...


# aiohttp
app = web.Application()
utran.set_server_instance(app)

async def home(request):
    try:
        result = await remote_add(2,3)
        return web.Response(text=str(result))
    except utran.UtExceptions.RemoteRunActionError as e:
        return web.Response(text=str(e))
    
async def stop(request):
    print("Server to stoped")
    utran.Host.instance().exit()
    return web.Response(text="Server stoped")


async def get_delay(request):
    res = await utran.getConnectionsDelay()
    return web.Response(text=str(res))



app.router.add_get('/',home)
app.router.add_get('/delay',get_delay)
app.router.add_get('/stop',stop)


utran.add_act_uri('ws://127.0.0.1:2525/utran')




# 启动方法1：
# utran.run(host='127.0.0.1',port=random.randint(2000,3000))


# 启动方法2：
utran.start_host()
web.run_app(app,host='127.0.0.1',port=random.randint(2000,3000))